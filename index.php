<?php require_once "./code.php"; ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>

    <h1>ACTIVITY</h1>
    <h1>Person</h1>
    <p><?= $person -> printName(); ?></p>

    <h1>Developer</h1>
    <p><?= $developer -> printName(); ?></p>

    <h1>Engineer</h1>
    <p><?= $engineer -> printName(); ?></p><br>


    <h1>SUPPLEMENTARY ACTIVITY</h1>
    <h1>Voltes V</h1>
    <p><?php var_dump($voltesMember); ?></p>   
    <p><?php var_dump($voltesMember1); ?></p>
    <p><?php var_dump($voltesMember2); ?></p>
    <p><?php var_dump($voltesMember3); ?></p>
    <p><?php var_dump($voltesMember4); ?></p>

    <hr>

    <p><?= $voltesMember -> printName(); ?></p>
    <p><?= $voltesMember1 -> printName(); ?></p>
    <p><?= $voltesMember2 -> printName(); ?></p>
    <p><?= $voltesMember3 -> printName(); ?></p>
    <p><?= $voltesMember4 -> printName(); ?></p>
    
</body>
</html>